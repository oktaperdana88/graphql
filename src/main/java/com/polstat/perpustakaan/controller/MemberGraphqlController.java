package com.polstat.perpustakaan.controller;

import com.polstat.perpustakaan.dto.MemberDto;
import com.polstat.perpustakaan.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class MemberGraphqlController {
    @Autowired
    private MemberService memberService;

    @MutationMapping
    public MemberDto createMember(@Argument String name, @Argument String memberID, @Argument String address, @Argument String phoneNumber) {
        MemberDto memberDto = MemberDto.builder()
                .name(name)
                .memberID(memberID)
                .address(address)
                .phoneNumber(phoneNumber)
                .build();
        return memberService.createMember(memberDto);
    };

    @MutationMapping
    public MemberDto updateMember(@Argument Long id,@Argument String name, @Argument String memberID, @Argument String address, @Argument String phoneNumber) {
        MemberDto memberDto = memberService.getMemberById(id);
        memberDto.setMemberID(memberID);
        memberDto.setName(name);
        memberDto.setAddress(address);
        memberDto.setPhoneNumber(phoneNumber);
        return memberService.updateMember(memberDto);
    };

    @MutationMapping
    public void deleteMember(@Argument Long id) {
        MemberDto memberDto = memberService.getMemberById(id);
        memberService.deleteMember(memberDto);
    };

    @QueryMapping
    public List<MemberDto> members() {
        return memberService.getMembers();
    };

    @QueryMapping
    public MemberDto memberByName(@Argument String name) {
        return memberService.getMemberByName(name);
    };

    @QueryMapping
    public MemberDto memberById(@Argument Long id) {
        return memberService.getMemberById(id);
    };

    @QueryMapping
    public MemberDto memberByMemberId(@Argument String memberID) {
        return memberService.getMemberByMemberID(memberID);
    };
}
