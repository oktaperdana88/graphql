package com.polstat.perpustakaan.rpc;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JsonRpcRequest {
    private String jsonRpc;
    private String method;
    private JsonNode params;
    private String id;
}
