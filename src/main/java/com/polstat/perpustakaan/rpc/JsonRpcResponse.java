package com.polstat.perpustakaan.rpc;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JsonRpcResponse {
    private String jsonRpc;
    private Object result;
    private Object error;
    private String id;

    public JsonRpcResponse(Object result, String id) {
        this.result = result;
        this.id = id;
    }
}
