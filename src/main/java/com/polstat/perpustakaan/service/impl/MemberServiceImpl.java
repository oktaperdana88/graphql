package com.polstat.perpustakaan.service.impl;

import com.polstat.perpustakaan.dto.MemberDto;
import com.polstat.perpustakaan.entity.Member;
import com.polstat.perpustakaan.mapper.MemberMapper;
import com.polstat.perpustakaan.repository.MemberRepository;
import com.polstat.perpustakaan.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MemberServiceImpl implements MemberService {
    @Autowired
    private MemberRepository memberRepository;
    @Override
    public MemberDto createMember(MemberDto memberDto) {
        Member member = memberRepository.save(MemberMapper.mapToMember(memberDto));
        return MemberMapper.mapToMemberDto(member);
    }

    @Override
    public MemberDto updateMember(MemberDto memberDto) {
        Member member = memberRepository.save(MemberMapper.mapToMember(memberDto));
        return MemberMapper.mapToMemberDto(member);
    }

    @Override
    public void deleteMember(MemberDto memberDto) {
        memberRepository.deleteById(memberDto.getId());
    }

    @Override
    public List<MemberDto> getMembers() {
        List<Member> members = memberRepository.findAll();
        List<MemberDto> memberDtos = members.stream()
                .map(MemberMapper::mapToMemberDto)
                .collect(Collectors.toList());
        return memberDtos;
    }

    @Override
    public MemberDto getMemberByName(String name) {
        Optional<Member> memberOptional = memberRepository.getMemberByName(name);
        if (memberOptional.isPresent()){
            Member member = memberOptional.get();
            return MemberMapper.mapToMemberDto(member);
        }
        return null;
    }

    @Override
    public MemberDto getMemberById(Long Id) {
        Optional<Member> memberOptional = memberRepository.findById(Id);
        if (memberOptional.isPresent()){
            Member member = memberOptional.get();
            return MemberMapper.mapToMemberDto(member);
        }
        return null;
    }

    @Override
    public MemberDto getMemberByMemberID(String id) {
        Optional<Member> memberOptional = memberRepository.getMemberByMemberID(id);
        if (memberOptional.isPresent()){
            Member member = memberOptional.get();
            return MemberMapper.mapToMemberDto(member);
        }
        return null;
    }
}
