package com.polstat.perpustakaan.mapper;

import com.polstat.perpustakaan.dto.MemberDto;
import com.polstat.perpustakaan.entity.Member;

public class MemberMapper {
    public static Member mapToMember (MemberDto memberDto) {
        Member member = Member.builder()
                .id(memberDto.getId())
                .name(memberDto.getName())
                .address(memberDto.getAddress())
                .memberID(memberDto.getMemberID())
                .phoneNumber(memberDto.getPhoneNumber())
                .build();
        return member;
    }

    public static MemberDto mapToMemberDto(Member member) {
        MemberDto memberDto = MemberDto.builder()
                .id(member.getId())
                .name(member.getName())
                .address(member.getAddress())
                .memberID(member.getMemberID())
                .phoneNumber(member.getPhoneNumber())
                .build();
        return memberDto;
    }
}
