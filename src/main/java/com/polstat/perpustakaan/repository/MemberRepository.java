package com.polstat.perpustakaan.repository;

import com.polstat.perpustakaan.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    @Query("SELECT m FROM Member m WHERE m.name = ?1")
    Optional<Member> getMemberByName(String name);
    @Query("SELECT m FROM Member m WHERE m.memberID = ?1")
    Optional<Member> getMemberByMemberID(String memberID);
}
